using System;
using System.Linq;

namespace pipe_test
{
    public static class Calculator
    {
        public static int Add(int x, params int[] rest)
        {
            return x + rest.Sum();
        }

        public static int Subtract(int x, params int[] rest)
        {
            return x - rest.Sum();
        }
    }
}