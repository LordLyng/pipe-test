using System;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace pipe_test.Test
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(1, 2, 3)]
        public void Test_Add(int x, params int[] rest)
        {
            var expected = x + rest.Sum();

            var result = Calculator.Add(x, rest);

            result.Should().Be(expected);
        }

        [Theory]
        [InlineData(1, 2, 3)]
        public void Test_Subtract(int x, params int[] rest)
        {
            var expected = x - rest.Sum();

            var result = Calculator.Subtract(x, rest);

            result.Should().Be(expected);
        }
    }
}
